<?php
/**
 * Plugin Name: WPezPlugins: Rank Math customizations for Alchemy United
 * Plugin URI: TODO
 * Description: Customizations for Rank Math that are unique to the AU project (+ also filters the RSS title)
 * Version: 0.0.1
 * Author: Mark "Chief Executive Alchemist" Simchock for Alchemy United
 * Author URI: https://AlchemyUnited.com
 * License: GPLv2 or later
 * Text Domain: TODO
 */

namespace RathMathCustomizationsForAU;

add_filter( 'rankmath/frontend/remove_credit_notice', __NAMESPACE__ . '\removeCreditnotice', PHP_INT_MAX, 1 );
function removeCreditnotice( $bool ) {
	// Return true to remove the credit notice.
	return true;
}

// Fixes the <title>, FB og:title and Twitter title on the frontend.
add_filter( 'rank_math/frontend/title', __NAMESPACE__ . '\rmFETitle', 10, 1 );
function rmFETitle( $title ) {

	global $post;

	if ( strpos( $post->post_title, '<br>' ) !== false ) {

		return feTitleAll();
	}

	return $title;
}

function feTitleAll() {
	global $post;
	return str_replace( '<br>', ' ', $post->post_title ) . ' | ' . get_bloginfo( 'name' );
}

// This updates the RM title admin side.
add_filter( 'rank_math/metabox/post/values', __NAMESPACE__ . '\rmValues', PHP_INT_MAX - 1, 2 );
function rmValues( $values, $obj ) {

	global $post;

	if ( isset( $values['assessor']['serpData']['title'] ) ) {

		// if the post title has a <br> in it, then we need to replace it with a space.
		// Else when the title is sanitized, the <br> will be removed and there will be no space between the two words.
		$values['assessor']['serpData']['title'] = str_replace( '<br>', ' ', $post->post_title );
	}
	return $values;
}
